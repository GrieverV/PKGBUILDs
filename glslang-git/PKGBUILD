# Maintainer: Griever <grieverv@local.host>

pkgname=glslang-git
pkgver=8.13.3559.r3591.965bd4d9
pkgrel=1
pkgdesc='OpenGL and OpenGL ES shader front end and validator (git version)'
arch=('x86_64')
url='https://github.com/KhronosGroup/glslang'
license=('BSD')
depends=('gcc-libs' 'spirv-tools' 'python')
makedepends=('cmake' 'ninja')
provides=('glslang')
conflicts=('glslang')
options=('staticlibs')
source=('git+https://github.com/KhronosGroup/glslang.git')
sha256sums=('SKIP')

pkgver() {
  cd glslang

  printf "%s.r%s.%s" "$(git describe --tags --abbrev=0 --match "[0-9]*" | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g')" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

build() {
  cd glslang
  mkdir -p build-{shared,static}
  (cd build-shared
    cmake .. \
      -GNinja \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      -DBUILD_SHARED_LIBS=ON
    ninja
  )
  (cd build-static
    cmake .. \
      -GNinja \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_BUILD_TYPE=Release \
      -DBUILD_SHARED_LIBS=OFF
    ninja
  )
}

package() {
  cd glslang
  DESTDIR="${pkgdir}" ninja -C build-shared install
  DESTDIR="${pkgdir}" ninja -C build-static install
  cd "${pkgdir}"/usr/lib
  for lib in *.so; do
    ln -sf "${lib}" "${lib}.0"
  done
}
