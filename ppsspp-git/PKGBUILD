# Maintainer: Griever <grieverv@local.host>

pkgbase=ppsspp-git
pkgname=('ppsspp-git')
pkgver=1.7.1.r225.c3bd37504
_ffmpegver=3.0
pkgrel=1
pkgdesc='A PSP emulator written in C++'
arch=('x86_64')
url='http://www.ppsspp.org/'
license=('GPL2')
depends=('gcc-libs' 'glew' 'glibc' 'libgl' 'sdl2' 'zlib')
makedepends=('cmake' 'git' 'glu' 'libglvnd' 'libzip')
source=('git+https://github.com/hrydgard/ppsspp.git'
        'git+https://github.com/Kingcom/armips.git'
        'git+https://github.com/discordapp/discord-rpc.git'
        'ppsspp-glslang::git+https://github.com/hrydgard/glslang.git'
        'git+https://github.com/hrydgard/ppsspp-lang.git'
        'git+https://github.com/Tencent/rapidjson.git'
        'git+https://github.com/KhronosGroup/SPIRV-Cross.git'
        'armips-tinyformat::git+https://github.com/Kingcom/tinyformat.git'
        "https://github.com/FFmpeg/FFmpeg/archive/release/${_ffmpegver}.tar.gz"
        'https://raw.githubusercontent.com/hrydgard/ppsspp-ffmpeg/master/linux_x86-64.sh'
        'https://github.com/gabomdq/SDL_GameControllerDB/raw/master/gamecontrollerdb.txt'
        'ppsspp.sh'
        'ppsspp.desktop'
        'sdl-fix-fullscreen-init.patch')
sha256sums=('SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            '2c2d1ee6d1ce5c2acec372d58b8079885f6d5d674633cfea489cd550252a5426'
            '1c332702d0aeced07df7e12ba8530bc3f19a52bc76c355f6c84c141becfd46d8'
            '4e8fd424f7d8388edc8a3bb120ba06c2fc49e78384fa546eb86440ea6c147400')

pkgver() {
  cd ppsspp

  echo "$(git describe --tags | sed 's/^v//; s/-/.r/; s/-g/./')"
}

prepare() {
  cd ppsspp

  patch -Np1 -i "${srcdir}/sdl-fix-fullscreen-init.patch"

  rm -f assets/gamecontrollerdb.txt
  cp "${srcdir}/gamecontrollerdb.txt" assets/gamecontrollerdb.txt

  rm -rf ffmpeg/
  ln -s "${srcdir}/FFmpeg-release-${_ffmpegver}" ffmpeg

  for submodule in assets/lang ext/glslang; do
    git submodule init ${submodule}
    git config submodule.${submodule}.url ../ppsspp-${submodule#*/}
    git submodule update ${submodule}
  done
  for submodule in ext/{SPIRV-Cross,armips,discord-rpc,rapidjson}; do
    git submodule init ${submodule}
    git config submodule.${submodule}.url ../${submodule#*/}
    git submodule update ${submodule}
  done

  pushd ext/armips

  for submodule in ext/tinyformat; do
    git submodule init ${submodule}
    git config submodule.${submodule}.url ../../../armips-${submodule#*/}
    git submodule update ${submodule}
  done

  popd

  chmod +x "${srcdir}/linux_x86-64.sh"
  cp "${srcdir}/linux_x86-64.sh" ffmpeg/linux_x86-64.sh

  if [[ -d build ]]; then
    rm -rf build
  fi
  mkdir build
}

build() {
  pushd ppsspp/ffmpeg
  ./linux_x86-64.sh
  popd

  cd ppsspp/build

  cmake .. \
    -DCMAKE_BUILD_TYPE='Release' \
    -DCMAKE_SKIP_RPATH='ON' \
    -DHEADLESS='OFF' \
    -DUSE_SYSTEM_LIBZIP='ON'
  make
}

package_ppsspp-git() {
  depends+=('hicolor-icon-theme' 'libzip')
  provides=('ppsspp')
  conflicts=('ppsspp' 'ppsspp-qt' 'ppsspp-qt-git')

  cd ppsspp/build

  install -dm 755 "${pkgdir}"/{opt/ppsspp,usr/{bin,share/{applications,icons,pixmaps}}}
  install -m 755 PPSSPPSDL "${pkgdir}"/opt/ppsspp/
  cp -dr --no-preserve='ownership' assets "${pkgdir}"/opt/ppsspp/
  cp -dr --no-preserve='ownership' ../icons/hicolor "${pkgdir}"/usr/share/icons/
  install -m 644 ../icons/icon-512.svg "${pkgdir}"/usr/share/pixmaps/ppsspp.svg
  install -m 755 ../../ppsspp.sh "${pkgdir}"/usr/bin/ppsspp
  install -m 644 ../../ppsspp.desktop "${pkgdir}"/usr/share/applications/
}
