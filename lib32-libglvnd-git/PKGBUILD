# Maintainer: Griever <grieverv@local.host>

pkgname=lib32-libglvnd-git
_gitname=lib32-libglvnd
pkgver=1.3.0.r734.cb7b1fb
pkgrel=1
pkgdesc="The GL Vendor-Neutral Dispatch library"
arch=('x86_64')
url="https://github.com/NVIDIA/libglvnd"
license=('custom:BSD-like')
makedepends=('git' 'lib32-libx11' 'lib32-libxext' 'xorgproto' 'python' 'meson')
provides=('lib32-libglvnd' 'lib32-libgl' 'lib32-libegl' 'lib32-libgles')
conflicts=('lib32-libglvnd')
source=("$_gitname::git+https://gitlab.freedesktop.org/glvnd/libglvnd.git"
        LICENSE)
sha512sums=('SKIP'
            'bf0f4a7e04220a407400f89226ecc1f798cc43035f2538cc8860e5088e1f84140baf0d4b0b28f66e4b802d4d6925769a1297c24e1ba39c1c093902b2931781a5')

pkgver() {
  cd "$srcdir/$_gitname"

  printf "%s.r%s.%s" "$(git describe --tags --abbrev=0 | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g')" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

build() {
  export CC='gcc -m32'
  export CXX='g++ -m32'
  export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

  arch-meson "$_gitname" build \
    --libdir=/usr/lib32 \
    -D headers=false

  ninja -C build
}

package() {
  # lib32-libglvnd needs lib32-mesa for indirect rendering
  depends=('lib32-libxext' 'libglvnd' 'lib32-mesa' 'lib32-opengl-driver')

  DESTDIR="$pkgdir" ninja -C build install

  rm -r "$pkgdir"/usr/include

  mkdir -p "$pkgdir"/usr/share/licenses
  ln -s libglvnd-git "$pkgdir"/usr/share/licenses/$pkgname
}
